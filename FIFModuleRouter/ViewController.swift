//
//  ViewController.swift
//  FIFModuleRouter
//
//  Created by Erick Reategui on 25-10-21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func didPressModule1(_ sender: UIButton) {
        let strategy =  Module1RouterExample()
        //PRESENT
//        FIFModuleRouter.shared?.goTo(origin: self, strategy: strategy, routeStyle: .present)
        
        //PUSH
        FIFModuleRouter.shared?.goTo(origin: self, strategy: strategy, routeStyle: .push)
    }
    
    @IBAction func didPressModule2(_ sender: UIButton) {
        let strategy =  Module2RouterExample()
        //PRESENT
        FIFModuleRouter.shared?.goTo(origin: self, strategy: strategy, routeStyle: .present)
        
        //PUSH
//        FIFModuleRouter.shared?.goTo(origin: self, strategy: strategy, routeStyle: .push)
    }
    
    @IBAction func didPressModule3(_ sender: UIButton) {
        let strategy =  Module3RouterExample()
        //PRESENT
        FIFModuleRouter.shared?.goTo(origin: self, strategy: strategy, routeStyle: .present)
        
        //PUSH
//        FIFModuleRouter.shared?.goTo(origin: self, strategy: strategy, routeStyle: .push)
    }
}

