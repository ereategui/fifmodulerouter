//
//  AppModuleRouter.swift
//  POCEventosWebView
//
//  Created by Erick Reategui on 22-10-21.
//

import UIKit

class AppModuleRouter {
    public static var shared: FIFModuleRouter<FIFAbstractStrategy>? = FIFModuleRouter<FIFAbstractStrategy>.shared
}

class FIFAbstractStrategy: FIFModuleRouterStrategyProtocol {
    func goTo(origin: UIViewController, routeStyle: FIFRouteStyle) {}
}

enum FIFRouteStyle {
    case present
    case push
}
