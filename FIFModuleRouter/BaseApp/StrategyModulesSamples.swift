//
//  AppModuleRouterDelegate.swift
//  POCEventosWebView
//
//  Created by Erick Reategui on 21-10-21.
//

import UIKit

class StrategyModulesSamples {}

class Module1RouterExample: FIFAbstractStrategy {
    
    override func goTo(origin: UIViewController, routeStyle: FIFRouteStyle) {
        let module1FirstVC = UIViewController()
        module1FirstVC.view.backgroundColor = .blue
        switch routeStyle {
        case .present:
            print("Present Module 1 VC!!!")
            origin.present(module1FirstVC, animated: true)
        case .push:
            print("Pushhhh Module 1 VC!!!")
            if origin is UINavigationController, let navigatior = origin as? UINavigationController {
                navigatior.pushViewController(module1FirstVC, animated: true)
            } else if let navigationController = origin.navigationController {
                navigationController.pushViewController(module1FirstVC, animated: true)
            }
            
        }
    }
}

class Module2RouterExample: FIFAbstractStrategy {
    override func goTo(origin: UIViewController, routeStyle: FIFRouteStyle) {
        let module2FirstVC = UIViewController()
        module2FirstVC.view.backgroundColor = .red
        switch routeStyle {
        case .present:
            print("Present Module 2 VC!!!")
//            module2FirstVC.modalPresentationStyle = .fullScreen
            origin.present(module2FirstVC, animated: true)
        case .push:
            print("Pushhhh Module 2 VC!!!")
            if origin is UINavigationController, let navigatior = origin as? UINavigationController {
                navigatior.pushViewController(module2FirstVC, animated: true)
            } else if let navigationController = origin.navigationController {
                navigationController.pushViewController(module2FirstVC, animated: true)
            }
        }
    }
}

class Module3RouterExample: FIFAbstractStrategy {
    override func goTo(origin: UIViewController, routeStyle: FIFRouteStyle) {
        let module3FirstVC = UIViewController()
        module3FirstVC.view.backgroundColor = .yellow
        switch routeStyle {
        case .present:
            print("Present Module 3 VC!!!")
            origin.present(module3FirstVC, animated: true)
        case .push:
            if origin is UINavigationController, let navigatior = origin as? UINavigationController {
                navigatior.pushViewController(module3FirstVC, animated: true)
            } else if let navigationController = origin.navigationController {
                navigationController.pushViewController(module3FirstVC, animated: true)
            }
        }
    }
}


