//
//  FIFModuleRouter.swift
//  POCEventosWebView
//
//  Created by Erick Reategui on 21-10-21.
//

import UIKit

public class FIFModuleRouter<ModuleRouterStrategy: FIFModuleRouterStrategyProtocol>: FIFModuleRouterProtocol {


    public static var shared: FIFModuleRouter<ModuleRouterStrategy>? {
        let store_key = String(describing: ModuleRouterStrategy.self)
        if let singleton = FIFModuleRouterDataStorage.singletons_storage[store_key] {
            return singleton as? FIFModuleRouter<ModuleRouterStrategy>
        } else {
            let new_singleton = FIFModuleRouter<ModuleRouterStrategy>()
            FIFModuleRouterDataStorage.singletons_storage[store_key] = new_singleton
            return new_singleton
        }
    }

    public func goTo<View: UIViewController>(origin: View, strategy: ModuleRouterStrategy, routeStyle: ModuleRouterStrategy.RouteStyle) {
        strategy.goTo(origin: origin, routeStyle: routeStyle)
    }

}
