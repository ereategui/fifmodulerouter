//
//  FIFModuleRouterProtocol.swift
//  POCEventosWebView
//
//  Created by Erick Reategui on 21-10-21.
//

import UIKit

//Implemented on FIFModuleRouter inside this module
protocol FIFModuleRouterProtocol: AnyObject {
    associatedtype ModuleRouterStrategy: FIFModuleRouterStrategyProtocol
    func goTo(origin: UIViewController, strategy: ModuleRouterStrategy, routeStyle: ModuleRouterStrategy.RouteStyle)
}

// Base definition of strategy pattern trigger.
public protocol FIFModuleRouterStrategyProtocol: AnyObject {
    associatedtype RouteStyle
    func goTo(origin: UIViewController, routeStyle: RouteStyle)
}

